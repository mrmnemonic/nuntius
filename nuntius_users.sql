create table users (
id integer primary key,
username VARCHAR(32) NOT NULL,
domain VARCHAR(32) NOT NULL,
registered datetime DEFAULT (DATETIME('now', 'localtime')),
email VARCHAR(128));
create table partners (
id integer primary key,
domain VARCHAR(32) NOT NULL,
key VARCHAR(32) NOT NULL,
contact VARCHAR(32) NOT NULL,
email VARCHAR(128),
registered datetime DEFAULT (DATETIME('now', 'localtime')));
