<?php
// Written 2014 By Martin Coleman.
// Sample new account join for Nuntius. Public Domain.
$domain="mydomain.com";
$key="b58ac503de970338fe7b84e3b339f9d6";
$url="http://nuntius.mydomain.com/nuntius.cgi";
if(!isset($_POST['username'])) { die("Error, no username specified."); }
if(!isset($_POST['password1'])) { die("Error, no password specified."); }
$username=$_POST['username'];
$password=$_POST['password1'];
$email=$_POST['email'];

$parms="action=register&domain=".$domain."&key=".$key."&username=".$username."&password=".$password."&email=".$email;
//echo "DEBUG: [".$parms."]\n<br>";

$ch=curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $parms);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response=curl_exec($ch);
curl_close($ch);

//echo "DEBUG: Response [".$response."]\n<br>";
if($response=="1")
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Lightning Note</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div id="mainsite">
<center><h1><a href="http://www.lightningnote.com">Lightning Note</a></h1></center>

<div style="width:600px;margin:auto">
<center><h1>Success!</h1></center>
<p>You have successfully joined. You may now sign in using any compatible client.</p>
<p>Your new account name is <b><?php echo $username."@".$domain; ?></b>. Your password is the one you chose on the previous page.</p>
<p>We recommend trying one of the following:
<table width="100%">
<tr><th>Program</th><th width="50">Cost</th><th>Platforms</th></tr>
<tr><td><a href="http://psi-im.org/">Psi</a></td><td align="center">Free</td><td>Windows, Linux, Mac OS X, FreeBSD</td></tr>
<tr><td><a href="http://www.pidgin.im/">Pidgin</a></td><td align="center">Free</td><td>Windows, Linux, FreeBSD</td></tr>
<tr><td><a href="http://www.xabber.org/">Xabber</a></td><td align="center">Free</td><td>Android</td></tr>
</table>
</p>
</div>

<p>&nbsp;</p>
</div>
<center><font size="-2">Powered by <a href="http://www.mktechresearch.info/nuntiustech.html" target="_blank">NuntiusTech</a></font></center>

</body>
</html>
<?php
} else {
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Lightning Note</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div id="mainsite">
<center><h1><a href="http://www.lightningnote.com">Lightning Note</a></h1></center>

<table width="100%" cellpadding=0 cellspacing=0>
<tr><td align="center">
<h1>Error!</h1>
<p>Sorry, there was a problem. Please try again. If this persists, please contact the administrator.</p>
</td></tr>
</table>

<p>&nbsp;</p>
</div>
<center><font size="-2">Powered by <a href="http://www.mktechresearch.info/nuntiustech.html" target="_blank">NuntiusTech</a></font></center>

</body>
</html>


<?php
}
?>
