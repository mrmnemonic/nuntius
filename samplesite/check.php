<?php
// Written 2014 By Martin Coleman.
// Sample username check for Nuntius. Public domain.
$domain="mydomain.com";
$key="b58ac503de970338fe7b84e3b339f9d6";
$url="http://nuntius.mydomain.com/nuntius.cgi";
if(!isset($_POST['username'])) { die("Error, no username specified."); }
$username=$_POST['username'];

$parms="action=check&domain=".$domain."&key=".$key."&username=".$username;

$ch=curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $parms);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response=curl_exec($ch);
curl_close($ch);

if($response=="1")
{
    echo "<img src=\"images/not_available.png\" align=\"absmiddle\"> <font color=\"red\">Not Available </font>";
}
if($response=="0")
{
    echo "<img src=\"images/available.png\" align=\"absmiddle\"> <font color=\"Green\"> Available </font>";
}
if($response=="256")
{
    echo "Command Error";
}
if($response=="255")
{
    echo "Remote DB Error";
}
if($response=="254")
{
    echo "System Error";
}
if($response=="253")
{
    echo "Key Error";
}
?>