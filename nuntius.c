/*
    Written 2014,2017 by Martin A. COLEMAN (www.martincoleman.com).
    This file, nuntius.c, a part of the NuntiusTech eJabberd Manager, is hereby
    released under the MIT license. See LICENSE for more information.

Impromptu change log
2014.02.25
- Initial prototype used to check if an account already exists.

2014.??.??
- Lots of functionality added.

2014.04.21
- Bug fixes

2014.05.02
- Dedicated to the public domain. Inspired by Rob Landley's efforts (www.landley.net)
and the developer's essay "Why Release Your Work As Public Domain".

2017.07.24
- Re-released under the MIT license. See LICENSE for details.


SQL for creation:
create table users (
id integer primary key,
username VARCHAR(32) NOT NULL,
domain VARCHAR(32) NOT NULL,
registered datetime DEFAULT (DATETIME('now', 'localtime')),
email VARCHAR(128));

create table partners (
id integer primary key,
domain VARCHAR(32) NOT NULL,
key VARCHAR(32) NOT NULL,
contact VARCHAR(32) NOT NULL,
email VARCHAR(128),
registered datetime DEFAULT (DATETIME('now', 'localtime')));

***********************
WARNING: This has NO
error checking yet. It
completely trusts all
input at all times.
Basic error checking
will be implemented.
***********************
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include "qdecoder/src/qdecoder.h"

#define SW_VERSION "0.92"
#define EJABBERDCTL "/usr/sbin/ejabberdctl"

/* for SQLite3 */
sqlite3 *db;
char *sql=NULL;
char *zErrMsg=0;
sqlite3_stmt *stmt;
int sql_status=0;
int rc=0;

/* for qdecoder */
qentry_t *req;

void return_status(int status)
{
    /* do some housekeeping */
    sqlite3_close(db);
    req->free(req);

    /* show me the status */
    printf("Content-type: text/html\n\n");
    if(status)
    {
        putchar('1');
    } else {
        putchar('0');
    }
    exit(0);
}

void show_error(char *msg)
{
    sqlite3_close(db);
    req->free(req);
    printf("Content-type: text/plain\n\n");
    printf("%s", msg);
    exit(0);
}

int get_user_id(char *username, char *domain)
{
    int results=0;
    sql=sqlite3_mprintf("SELECT id FROM users WHERE username='%s' and domain='%s'", username, domain);
    rc=sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
    rc=sqlite3_step(stmt);
    if (rc==SQLITE_ROW)
        results = sqlite3_column_int(stmt, 0);
    rc=sqlite3_finalize(stmt);
    return results;
}

int confirm_partner(char *domain, char *key)
{
    int results=0;
    sql=sqlite3_mprintf("SELECT id FROM partners WHERE domain='%s' and key='%s'", domain, key);
    rc=sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
    rc=sqlite3_step(stmt);
    if (rc==SQLITE_ROW)
        results = sqlite3_column_int(stmt, 0);
    rc=sqlite3_finalize(stmt);
    return results;
}

void create_new_account(char *domain, char *username, char *password, char *email)
{
    FILE *fp;
    char register_cmd[256]={0};
    char results[128];

#ifdef TESTING
    fp=fopen("/tmp/nuntius.txt", "a");
    if(fp==NULL)
    {
        //show_error("Error writing to test file");
        return_status(0);
    }
    fprintf(fp, "domain=%s,username=%s,password=%s,email=%s\n", domain, username, password, email);
    fclose(fp);
    return_status(1);
#else
    /* create our record */
    sql=sqlite3_mprintf("INSERT INTO users (username, domain, email) VALUES('%s', '%s', '%s')", username, domain, email);
    rc=sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        printf("SQL Error: %s<br>", sqlite3_errmsg(db));
    }
    sqlite3_free(sql);

    /* talk to ejabberd */
    sprintf(register_cmd, "sudo %s register %s %s %s", EJABBERDCTL, username, domain, password);
    fp=popen(register_cmd, "r");
    //fscanf(fp, "%s[^\n]", results, NULL);
    fgets(results, 127, fp);
    fclose(fp);
    if(!strstr(results, "successfully"))
    {
        return_status(0);
    } else {
        return_status(1);
    }
#endif
}

void delete_account(char *domain, char *username)
{
    FILE *fp;
    char register_cmd[256]={0};
    char results[128];

#ifdef TESTING
    fp=fopen("/tmp/nuntius.txt", "a");
    if(fp==NULL)
    {
        //show_error("Error writing to test file");
        return_status(0);
    }
    fprintf(fp, "domain=%s,username=%s\n", domain, username, password);
    fclose(fp);
    return_status(1);
#else
    /* delete our record */
    sql=sqlite3_mprintf("DELETE FROM users WHERE username='%s' AND domain='%s' LIMIT 1", username, domain);
    rc=sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        printf("SQL Error: %s<br>", sqlite3_errmsg(db));
    }
    sqlite3_free(sql);

    /* tell ejabberd */
    sprintf(register_cmd, "sudo %s unregister %s %s", EJABBERDCTL, username, domain);
    fp=popen(register_cmd, "r");
    //fscanf(fp, "%s[^\n]", results, NULL);
    fgets(results, 127, fp);
    fclose(fp);
    if(!strstr(results, "successfully"))
    {
        return_status(0);
    } else {
        return_status(1);
    }
#endif
}

int main()
{
    char *action;
    char *domain;
    char *key;
    char *username;
    char *password;
    char *email;

    /* init qdecoder */
    req = qcgireq_parse(NULL, 0);

    /* get our command */
    action = (char *)req->getstr(req, "action", false);
    if(action==NULL) /* nothing? quit immediately. */
    {
        show_error("256");
        return 0;
    };

    /* ok, let's work on this database */
    rc=sqlite3_open("../nuntius_users.sq3", &db);
    if(rc)
	{
        /*
        printf("Content-type: text/html\n\n");
        printf("Database Error\n");
		sqlite3_close(db);
        req->free(req);
        */
        show_error("255");
		return 0;
	}
    
    domain = (char *)req->getstr(req, "domain", false);
    key = (char *)req->getstr(req, "key", false);
    if(domain==NULL || key==NULL)
    {
        /*
        printf("Content-type: text/html\n\n");
        req->free(req);
        printf("255");
        */
        show_error("254");
        return 0;        
    }
    
    if(!confirm_partner(domain, key))
    {
        /*
        printf("Content-type: text/html\n\n");
        req->free(req);
        printf("254");
        */
        show_error("253");
        return 0;
    }
    
    /* get the intended username */
    username = (char *)req->getstr(req, "username", false);

    /* do preliminary checks */
    if (username == NULL) { return_status(1); }
    if (strlen(username)<3) { return_status(1); }

    /* Checks for reserved names. Add more if preferred. */
    if (!strcmp(username, "admin")) { return_status(1); }
    if (!strcmp(username, "support")) { return_status(1); }
    if (!strcmp(username, "tech")) { return_status(1); }

    /* is it just a check? fine, nice and easy */
    if(!strcmp(action, "check"))
    {
        if(get_user_id(username, domain))
        {
            return_status(1);
        } else {
            return_status(0);
        }
    }
    if(!strcmp(action, "register"))
    {
        password = (char *)req->getstr(req, "password", false);
        email = (char *)req->getstr(req, "email", false);
        create_new_account(domain, username, password, email);
    }
    if(!strcmp(action, "delete"))
    {
        delete_account(domain, username);
    }
	return 0;
}
